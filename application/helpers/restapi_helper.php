<?php 


function curl_post($url,$token,$data){
        
        $params = '';   
        foreach($data as $key=>$value)
                $params .= $key.'='.$value.'&';
         
        $params = trim($params, '&');

        $headers=[
            'akses_token:'.$token,
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        
		curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close ($ch);
		$data = ['data'=>	$server_output,'code'=>$httpcode];
		return $data;
	 
}
function curl_get($url,$token,$data=[]){
  
    $params = '';
    foreach($data as $key=>$value)
            $params .= $key.'='.$value.'&';
     
    $params = trim($params, '&');
    
    
 
    $headers=[
        'akses_token:'.$token,
    ];
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   
    $data = ['data'=>$result,'code'=>$httpcode];
    return $data;
}
function curl_del($path,$token)
{

     $headers=[
        'akses_token:'.$token,
    ];
    $url = $path;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   
    $data = ['data'=>$result,'code'=>$httpcode];
    return $data;
}
 function curl_put($path,$token,$data=[])
{

    $params = '';
    foreach($data as $key=>$value)
            $params .= $key.'='.$value.'&';
     
    $params = trim($params, '&');
    
    
     $headers=[
        'akses_token:'.$token,
    ];

    $url = $path;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   
    $data = ['data'=>$result,'code'=>$httpcode];
    return $data;
}
function get_status($id){
    switch($id){
        case 0 : return 'terikirim'; break;
        case 1 : return 'pengkajian'; break;
        case 2 : return 'diterima'; break;
        case 99 : return 'ditolak'; break;
    }
}