<div class="row">
  <div class="col col-md-12">

    <div class="card mt-5 col col-md-12">
      <div class="card-body">
        <h3 class="card-title">E-Report Parking Rumah Sakit Daerah Pemerintah Kota Kotamobagu</h3>
        <h6 class="card-subtitle mb-2 text-muted">Realtime Report</h6>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
       </div>
    </div>
      <div class="col col-md-12 mt-5">
        <div class="row">
          <div class="col col-md-6 p-5" style="background:#f39c12;color:#FFF">
                <span style='font-size:22px'>Tahun Lalu</span>
                 <h2 id='t-kemaren'>0</h2>
          </div>
          <div class="col col-md-6 p-5" style="background:#34495e;color:#FFF">
                <span style='font-size:22px'>Tahun Ini</span>
                 <h2 id='t-ini'>0</h2>
          </div>
          <div class="col col-md-4 p-5" style="background:#e74c3c;color:#FFF">
                <span style='font-size:22px'>Bulan Kemaren</span>
                 <h2 id='b-kemaren'>0</h2>
          </div>
          <div class="col col-md-4 p-5" style="background:#1abc9c;color:#FFF">
                <span style='font-size:22px'>Bulan Ini</span>
                <h2 id='b-ini'>0</h2>
          </div>
          <div class="col col-md-4 p-5" style="background:#3498db;color:#FFF">
                <span style='font-size:22px'>Hari Ini</span>
                <h2 id='hari-ini'>0</h2>
          </div>
        </div>
    </div>
       <br />
      <br />
      <h4>Jumlah Transaksi Tahun <?=date('Y');?> Berdasarkan Kendaraan </h4>
      <table id='tabel-jenis' class="mt-2 table table-striped table-hover table-bordered">
       <thead>
         <tr>
           <th width=10>#</th>
           <th>Jenis Kendaraan</th>
           <th>Jumlah Transaksi</th>
           <th>Total Transaksi</th>
         </tr>
       </thead>
       <tbody>

       </tbody>

     </table>
  </div>
</div>
