<div class="row">
  <div class="col col-md-12">

    <div class="card mt-5 col col-md-12">
      <div class="card-body">
        <h3 class="card-title">Laporan Pendapatan</h3>
        <h6 class="card-subtitle mb-2 text-muted">Realtime Report</h6>
        </div>
    </div>
    <div class="row col-md-12 mt-5">
        <form id='form-cari'  class="form-inline form float-right">
          <div class="form-group">
            <select name="" id="tahun" class="form-control">
              <option disabled selected>Pilih Tahun</option>
              <?php

              for($i=2019;$i<=date('Y');$i++){
              ?>
              <option value="<?=$i;?>"><?=$i;?></option>
              <?php
              }
              ?>
             </select>
          </div>
          <div class="form-group">
            <button type='button' id='cari-data' class="btn btn-primary"> Lihat</button>
          </div>
        </form>

         <table id='tabel' class="mt-5 table table-striped table-hover table-bordered">
          <thead>
            <tr>
              <th width=10>#</th>
              <th>Bulan</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>

          </tbody>

        </table>

        <button type='button' id='kembali' class="btn btn-primary" style='display:none'> Kembali </button>

        <table id='tabel2' class="mt-5 table table-striped table-hover table-bordered" style='display:none'>
         <thead>
           <tr>
             <th width=10>#</th>
             <th>Tanggal</th>
             <th>Total</th>
           </tr>
         </thead>
         <tbody>

         </tbody>

       </table>
    </div>

  </div>
</div>
