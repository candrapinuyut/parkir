<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Data_Model','dt');
		date_default_timezone_set('Asia/Makassar');
	}
	public function index(){
		  $data['view']='home/home';
			$data['data']=[];
			$this->load->view('master',$data);
	}
	public function laporan(){
		$data['view']='laporan/index';
		$data['data']=[];
		$this->load->view('master',$data);
	}

	function harian_singel(){
		$date=date('Y-m-d');
		$d = $this->db->query("select sum(bayar) as total from keluartotal where masuk like '$date%'");
		$d = $d->row();
		$dd=[];
		$dd['total']=number_format($d->total,0);
		echo json_encode($dd);
	}
	function tahun_singel($tahun){
		$d = $this->db->query('select sum(bayar) as total from keluartotal where YEAR(masuk)='.$tahun);
		$d = $d->row();
		$dd=[];
		$dd['total']=number_format($d->total,0);
		echo json_encode($dd);
	}
	function bulan_singel($tahun,$bulan){
		$d = $this->db->query('select sum(bayar) as total from keluartotal where YEAR(masuk)='.$tahun.' and MONTH(masuk)='.$bulan);
		$d = $d->row();
		$dd=[];
		$dd['total']=number_format($d->total,0);
		echo json_encode($dd);
	}
	function tahun_all($tahun){
		$query= $this->db->query('select sum(bayar) as total,year(masuk) as tahun,month(masuk) as bulan from keluartotal where YEAR(masuk)='.$tahun.' group by MONTH(masuk)');
		$d = $query->result();
		$dd=[];
 		$t=0;
		foreach($d as $i=>$r){
			$dd[$i]['tahun']=$r->tahun;
			$dd[$i]['bulan']=$r->bulan;
			$dd[$i]['total']=number_format($r->total,0);
			$t+=$r->total;
 		}
		$dd['total']=number_format($t,0);
		echo json_encode($dd);
	}
	function harian_all($tahun,$bulan){
		$query= $this->db->query('select sum(bayar) as total,year(masuk) as tahun,month(masuk) as bulan,day(masuk) as hari from keluartotal where YEAR(masuk)='.$tahun.'
		and MONTH(masuk)='.$bulan.'
		 group by day(masuk)');
		$d = $query->result();
		$dd=[];
 		$t=0;
		foreach($d as $i=>$r){
			$dd[$i]['tahun']=$r->tahun;
			$dd[$i]['bulan']=$r->bulan;
			$dd[$i]['tgl']=$r->hari;
			$dd[$i]['date']=$r->tahun.'-'.$r->bulan.'-'.$r->hari;
			$dd[$i]['date2']=$r->hari.'-'.$r->bulan.'-'.$r->tahun;
			$dd[$i]['total']=number_format($r->total,0);
			$t+=$r->total;
 		}
		$dd['total']=number_format($t,0);
		echo json_encode($dd);
	}

}
