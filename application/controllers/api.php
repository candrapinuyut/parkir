<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Data_Model','dt');
		date_default_timezone_set('Asia/Makassar');
	}
	function statistik_kendaraan(){
		$date=date('Y-m-d');
		$d = $this->db->query("SELECT COUNT( * ) jumlah, jenisk, SUM( bayar ) total
    FROM keluartotal
      WHERE YEAR( masuk ) =".date('Y')." GROUP BY jenisk");
		$d = $d->result();
    $data=[];
    $i=0;
    $t=0;
    foreach($d as $r){
      if($r->jenisk!=''){
        $data[$i]['jumlah']=number_format($r->jumlah);
        $data[$i]['total']=number_format($r->total);
        $data[$i]['jenis']=$r->jenisk;
        $t+=$r->total;
        $i++;
      }
    }
  		echo json_encode(['data'=>$data,'total'=>number_format($t)]);

	}
	function harian_singel(){
		$date=date('Y-m-d');
		$d = $this->db->query("select sum(bayar) as total from keluartotal where masuk like '$date%'");
		$d = $d->row();
		$dd=[];
		return number_format($d->total,0);

	}
	function tahun_singel($tahun){
		$d = $this->db->query('select sum(bayar) as total from keluartotal where YEAR(masuk)='.$tahun);
		$d = $d->row();
		$dd=[];
		return number_format($d->total,0);

	}
	function bulan_singel($tahun,$bulan){
		$d = $this->db->query('select sum(bayar) as total from keluartotal where YEAR(masuk)='.$tahun.' and MONTH(masuk)='.$bulan);
		$d = $d->row();
		$dd=[];
		return number_format($d->total,0);
 	}
	function tahun_all($tahun){
		$query= $this->db->query('select sum(bayar) as total,year(masuk) as tahun,month(masuk) as bulan from keluartotal where YEAR(masuk)='.$tahun.' group by MONTH(masuk)');
		$d = $query->result();
		$dd=[];
 		$t=0;
    $bul = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		foreach($d as $i=>$r){
			$dd[$i]['tahun']=$r->tahun;
			$dd[$i]['bulan']=$bul[$r->bulan];
			$dd[$i]['bulan2']=$r->bulan;
			$dd[$i]['total']=number_format($r->total,0);
			$t+=$r->total;
 		}
    $data=[];
    $data['total']=number_format($t,0);
    $data['data']=$dd;
    echo json_encode($data);
	}
	function harian_all($tahun,$bulan){
		$query= $this->db->query('select sum(bayar) as total,year(masuk) as tahun,month(masuk) as bulan,day(masuk) as hari from keluartotal where YEAR(masuk)='.$tahun.'
		and MONTH(masuk)='.$bulan.'
		 group by day(masuk)');
		$d = $query->result();
		$dd=[];
 		$t=0;
    $bul = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		foreach($d as $i=>$r){
			$dd[$i]['tahun']=$r->tahun;
			$dd[$i]['bulan']=$r->bulan;
			$dd[$i]['tgl']=$r->hari;
			$dd[$i]['date']=$r->tahun.'-'.$r->bulan.'-'.$r->hari;
			$dd[$i]['date2']=$r->hari.' '.$bul[$r->bulan].' '.$r->tahun;
			$dd[$i]['total']=number_format($r->total,0);
			$t+=$r->total;
 		}
    $data=[];
		$data['total']=number_format($t,0);
    $data['data']=$dd;
		echo json_encode($data);
	}
  function statistik(){
      $harian  = self::harian_singel();
      $bulan   = self::bulan_singel(date('Y'),date('m'));
      $bulanM1 = self::bulan_singel(date('Y'),date('m')-1);
      $tahun = self::tahun_singel(date('Y'));
      $tahunM1 = self::tahun_singel(date('Y')-1);

      $data=[];
      $data['tahun']=$tahun;
      $data['tahunM1']=$tahunM1;
      $data['harian']=$harian;
      $data['bulan']=$bulan;
      $data['bulanM1']=$bulanM1;
      echo json_encode($data);

  }
  function realtime_hari_ini(){
    $harian  = self::harian_singel();
    $bulan   = self::bulan_singel(date('Y'),date('m'));
    $data['bulan']=$bulan;
    $data['harian']=$harian;
    echo json_encode($data);
  }
}
