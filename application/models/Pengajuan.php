<?php
use GuzzleHttp\Client;
class Pengajuan extends CI_model {

	private $_client;
	private $_token;
	

	public function __construct()
	{
		$this->_client = new Client([
			'base_uri' => $this->config->item('apiurl'),
		]);
	}

	// fungsi untuk melakukan select data
	public function getAllData($token)
	{

		$res = $this->_client->request('GET', 'pengajuan', [
				    'headers' => [
				        'Accept'     => 'application/json',
				        'akses_token' => $token
				    ]
				]);

		$resp = $res->getBody()->getContents();
		$resp = json_decode($resp,true);
		return $resp['data'];

	}
	public function tes(){
		
	}

}