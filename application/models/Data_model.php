<?php 

 
class Data_model extends CI_Model{
 
    function __construct(){
        parent::__construct();
    }
	public function g_data(){
		return $this->db->query('select * from sipaham_data  order by tanggal_uppload desc')->result();
	}
	public function g_data_skpd($id){
		return $this->db->query('
		
		SELECT b.nama_file,b.tanggal_deadline,a.*,c.skpd,a.id id_data FROM sipaham_keb_data a 
		LEFT JOIN sipaham_data b on a.id_keb_data=b.id 
		LEFT JOIN tb_users c on a.id_skpd=c.id_user
		WHERE a.status=1 and a.id_keb_data='.$id.' order by b.tanggal_uppload desc')->result();
	}	
	public function g_data_skpd_byid($id){
		return $this->db->query('
		
		SELECT *,b.tanggal_uppload tgl_publish, a.id id_data FROM sipaham_keb_data a 
		LEFT JOIN sipaham_data b on a.id_keb_data=b.id 
		LEFT JOIN tb_users c on a.id_skpd=c.id_user
		WHERE a.status=1 and a.id_skpd='.$id.' order by b.tanggal_uppload desc')->result();
	}
	public function g_data_row($id){
		return $this->db->where('id',$id)->get('sipaham_data')->row();
	}
	public function store($data,$dinas){
		$this->store_dinas($data['id'],$dinas);
		return $this->db->insert('sipaham_data',$data);
		
	}	
	public function upload($id,$data){
		return $this->db->where('id',$id)->update('sipaham_keb_data',$data);
		
	}
	public function update($data,$id,$dinas){
		$this->update_dinas($id,$dinas);
		return $this->db->where('id',$id)->update('sipaham_data',$data);
		
	}
	public function store_dinas($id,$data){
		foreach($data as $d){
			$dt=[
				'id_keb_data'=>$id,
				'id_skpd'=>$d,
				'file'=>null,
				'tgl_upload'=>null,
				'status'=>1,
			];
			$this->db->insert('sipaham_keb_data',$dt);
		}
	}	
	public function update_dinas($id,$data){

		$cek = $this->db->where('id_keb_data',$id)->update('sipaham_keb_data',['status'=>0]);
		foreach($data as $d){
			
			$man_cek = $this->db->where('id_keb_data',$id)->where('id_skpd',$d)->get('sipaham_keb_data')->num_rows();
			$dt=[
					'id_keb_data'=>$id,
					'id_skpd'=>$d,
					'file'=>null,
					'tgl_upload'=>null,
					'status'=>1,
			];			
			if( $man_cek > 0 ){
		
				$this->db->where('id_keb_data',$id)->where('id_skpd',$d)->update('sipaham_keb_data',$dt);				
			}else{
			 
				$this->db->insert('sipaham_keb_data',$dt);				
			}

		}
	}
	public function hapus($id){
		$dt = [
			'status'=>0,
		];
		$this->db->where('id_keb_data',$id)->update('sipaham_keb_data',$dt);
		return $this->db->where('id',$id)->delete('sipaham_data');
	}
}