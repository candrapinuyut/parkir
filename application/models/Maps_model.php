<?php
use GuzzleHttp\Client;
class Maps_model extends CI_model {

	private $_client;
	private $_token;
	

	public function __construct()
	{
		$this->_client = new Client([
			'base_uri' => $this->config->item('apiurl'),
		]);
		$this->_token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7ImlkX3VzZXIiOiIxIiwidXNlcm5hbWUiOiJiYXBwZWRhIiwibGV2ZWwiOiIxIn0sIm9wZCI6eyJpZF9vcGQiOiI0IiwibmFtYV9vcGQiOiJCQURBTiBQRVJFTkNBTkFBTiBEQU4gUEVNQkFOR1VOQU4gREFFUkFIIiwiYWxhbWF0X29wZCI6Ik1vZ29sYWluZywgS290YW1vYmFndSBCYXIuLCBLb3RhIEtvdGFtb2JhZ3UsIFN1bGF3ZXNpIFV0YXJhIiwiaWRfdXNlciI6IjEifX0.WzUuQ0ro0P4HFXQGN9cHyjoFf4Oa8lvBnwRUVr-aX-w";
	}

	// fungsi untuk melakukan select data
	public function getAllData()
	{
		$token = $this->_token;
		$res = $this->_client->request('GET', 'pengajuan', [
				    'headers' => [
				        'Accept'     => 'application/json',
				        'akses_token' => $token
				    ]
				]);

		$resp = $res->getBody()->getContents();
		$resp = json_decode($resp,true);
		$resp = json_encode($resp['data'],JSON_PRETTY_PRINT);
		return $resp;

	}
	public function tes(){
		
	}

}
