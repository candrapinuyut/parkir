<?php 

use GuzzleHttp\Client;

class Laporan_model extends CI_Model{
    //var $host='http://kotamobagukota.go.id/apps/sipetarung/api/';
    var $host;

    var $token;
    function __construct(){
        parent::__construct();
        $this->token = $this->session->userdata('token');
		
		$this->_client = new Client([
			'base_uri' => $this->config->item('apiEplanning'),
		]);
		$this->host = $this->config->item('apiurl');
		
    }
	function get_all(){
		$g = curl_get($this->host.'laporan',$this->token);
        $data = json_decode($g['data']);
        return ($g['code']==200)?$data->data:false;
	}	
	function get_opd(){
		$g = curl_get($this->host.'opd',$this->token);
        $data = json_decode($g['data']);
        return ($g['code']==200)?$data->data:false;
	}	
	 
}