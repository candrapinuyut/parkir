<?php 

use GuzzleHttp\Client;

class Pengajuan_model extends CI_Model{
    //var $host='http://kotamobagukota.go.id/apps/sipetarung/api/';
 
    var $token;
    function __construct(){
        parent::__construct();
        $this->token = $this->session->userdata('token');

		$this->host = $this->config->item('apiurl');
		
		$this->_client = new Client([
			'base_uri' => $this->config->item('apiEplanning'),
		]);
		
		
    }
	function get_kegiatan( $id=null ){
		$c="";
		if( $id )
			$c="&idskpd=".$id;
		
		$res = $this->_client->request('GET', '?act=renjaAkhir'.$c, [
				    'headers' => [
				     'Accept'     => 'application/json',
 				    ]
				]);

		$resp = $res->getBody()->getContents();
		$resp = json_decode($resp,true);
		return $resp;	
			
	}
    	
	function get_kategori($id=null){
       $g = curl_get($this->host.'kategori/'.$id,$this->token);
        $data = json_decode($g['data']);
        return ($g['code']==200)?$data->data:false;
	}
    function get_all(){
        return curl_get($this->host.'pengajuan',$this->token);
    }
    function get_opd(){
        return curl_get($this->host.'opd',$this->token);
    }    
    function get_row( $id ){
        $g = curl_get($this->host.'pengajuan/'.$id,$this->token);
        $data = json_decode($g['data']);
        return ($g['code']==200)?$data->data:false;
    }    
	function get_history( $id ){
        $g = curl_get($this->host.'pengajuan/history/'.$id,$this->token);
        $data = json_decode($g['data']);
        return ($g['code']==200)?$data->data:false;
    }
    function hapus( $id ){
        $g = curl_del($this->host.'pengajuan/'.$id,$this->token);
        $data = json_decode($g['data']);
        return ($g['code']==200)?true:false;
    }    
    function ubahStatus( $id,$st ){
        $data = [
            'status'=>$st,
        ];
        $g = curl_put($this->host.'pengajuan/step_pengajuan/'.$id,$this->token,$data);
        $data = json_decode($g['data']);
        return ($g['code']==200)?true:false;

    }   
	function tolak( $id,$data ){
     
        $g = curl_put($this->host.'pengajuan/step_pengajuan/'.$id,$this->token,$data);
        $data = json_decode($g['data']);
        return ($g['code']==200)?true:false;

    }
    function store( $data ){
 
        $g = curl_post($this->host.'pengajuan',$this->token,$data);
        $data = json_decode($g['data']);
        return ($g['code']==200)?true:$g;
    }
}