
  function getHarian(e,y){
    var uri = $("#uri").val();
    $("#form-cari").hide();
    $("#tabel").hide();

    $("#kembali").show();
    $("#tabel2").show();
    $.ajax({
      url:uri+"api/harian_all/"+e+"/"+y,
      type:"GET",
      dataType:"json",
      beforeSend:function(){
        $("#tabel2 tbody").html('<tr><td colspan=3>Loading...</td></tr>');
      },
      success:function(msg){
        //console.log(msg)
        if(msg.data.length>0){
          var html="";
          msg.data.map(function(x,y){ y++;
            html+="<tr>"+
                    "<td>"+y+"</td>"+
                    "<td>"+x.date2+"</td>"+
                    "<td>"+x.total+"</td>"+
                  "</tr>";
          })
          html+="<tr>"+
                   "<th colspan=2>Total</th>"+
                  "<th>"+msg.total+"</th>"+
                "</tr>";
          $("#tabel2 tbody").html(html);
        }
        //console.log(msg.length);
      }
    })
  }
  function g_statistik(){
    var uri = $("#uri").val();
    $.ajax({
      url:uri+"api/statistik",
      type:"GET",
      dataType:"json",
      beforeSend:function(){
        $("#b-kemaren,#t-ini,#t-kemaren,#b-ini,#hari-ini").html("Load...");
      },
      success:function(msg){
        $("#b-kemaren").html("Rp. "+msg.bulanM1);
        $("#b-ini").html("Rp. "+msg.bulan);
        $("#t-ini").html("Rp. "+msg.tahun);
        $("#t-kemaren").html("Rp. "+msg.tahunM1);
        $("#hari-ini").html("Rp. "+msg.harian);
      }
    })
  }
  function g_jenis_kend(){
    var uri = $("#uri").val();
    $.ajax({
      url:uri+"api/statistik_kendaraan",
      type:"GET",
      dataType:"json",
      beforeSend:function(){
        $("#tabel-jenis tbody").html('<tr><td colspan=4>Loading...</td></tr>');
      },
      success:function(msg){
        if(msg.data.length>0){
          var html="";
          msg.data.map(function(x,y){ y++;
            html+="<tr>"+
                    "<td>"+y+"</td>"+
                    "<td >"+x.jenis+"</td>"+
                    "<td>"+x.jumlah+" Kali</td>"+
                    "<td>Rp. "+x.total+"</td>"+
                  "</tr>";
          })
          html+="<tr>"+
                   "<th colspan=3>Total</th>"+
                  "<th>Rp. "+msg.total+"</th>"+
                "</tr>";
          $("#tabel-jenis tbody").html(html);
        }
      }
    })
  }
  function realtime_hari_ini(){
    var uri = $("#uri").val();
    $.ajax({
      url:uri+"api/realtime_hari_ini",
      type:"GET",
      dataType:"json",
      success:function(msg){
        $("#hari-ini").html("Rp. "+msg.harian);
        $("#b-ini").html("Rp. "+msg.bulan);
      }
    })
  }
 $(function(){
       g_statistik();
       g_jenis_kend();
       setInterval(function(){
        realtime_hari_ini();
      },30000);
      $(".harian").click(function(){
          var l = $(this).attr('data-bulan');
          alert(l);
      })
      $("#kembali").click(function(){
        $("#tabel").show();
        $("#tabel2").hide();
        $("#form-cari").show();
        $("#kembali").hide();

      })
      $("#cari-data").click(function(){

          $("#tabel").show();
          $("#tabel2").hide();
          $("#kembali").hide();
          var uri = $("#uri").val();
          var tahun = $("#tahun").val();
          if(tahun==='' || tahun===null ){
            return false;
          }else{
            $.ajax({
              url:uri+"api/tahun_all/"+tahun,
              type:"GET",
              dataType:"json",
              beforeSend:function(){
                $("#tabel tbody").html('<tr><td colspan=3>Loading...</td></tr>');
              },
              success:function(msg){
                //console.log(msg)
                if(msg.data.length>0){
                  var html="";
                  msg.data.map(function(x,y){ y++;
                    html+="<tr style='cursor:pointer' class='harian' onclick='getHarian("+tahun+","+x.bulan2+")'>"+
                            "<td>"+y+"</td>"+
                            "<td  >"+x.bulan+"</td>"+
                            "<td>"+x.total+"</td>"+
                          "</tr>";
                  })
                  html+="<tr>"+
                           "<th colspan=2>Total</th>"+
                          "<th>"+msg.total+"</th>"+
                        "</tr>";
                  $("#tabel tbody").html(html);
                }
                //console.log(msg.length);
              }
            })
          }
      })

  })
