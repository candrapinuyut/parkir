<?php 

$q = mysqli_connect('localhost','root','','bappeda_db');


$qry = "SELECT   pd.PrioritasDaerah,spd.SasaranPrioritasDaerah,skpd.IdSkpd,bp.BankProgram,ra.Kegiatan,ra.DetailLokasi,
ra.Pagu,ra.PaguN1,ra.Keterangan,lra.IdRenjaAkhir,COUNT(lra.IdRenjaAkhir) AS Jumlah,bp.IdBankProgram,pd.IdPrioritasDaerah,spd.IdSasaranPrioritasDaerah

FROM tbl_prog_rpjmd_skpd prs, tbl_prog_prioritas_rpjmd ppr, tbl_bank_program bp,tbl_sasaran_prioritas_daerah spd, 
tbl_prioritas_daerah pd, tbl_prog_prioritas_daerah ppd,tbl_prog_rkpd pr,
tbl_sasaran s, tbl_tujuan t, tbl_misi m, tbl_visi v,
tbl_renja_akhir ra,tbl_lokasi_renja_akhir lra, tbl_lokasi l
, tbl_skpd skpd
WHERE prs.IdProgram5a=pr.IdProgram5a AND ppd.IdProgPrioritasDaerah=pr.IdProgPrioritasDaerah AND
prs.IdProgram5sa=ppr.IdProgram5sa AND ppr.IdBankProgram=bp.IdBankProgram AND prs.IdKantor=1 AND 
ppd.IdSasaranPrioritasDaerah=spd.IdSasaranPrioritasDaerah AND spd.IdPrioritasDaerah=pd.IdPrioritasDaerah
AND pd.Tahun=2019 AND ppr.IdProgram5sa=ppd.IdProgram5sa
AND ppr.IdSasaran=s.IdSasaran AND s.IdTujuan=t.IdTujuan AND t.IdMisi=m.IdMisi AND m.IdVisi=v.IdVisi
AND pr.IdProgramRkpd=ra.IdProgramRkpd AND ra.IdStatus=2
AND ra.IdRenjaAkhir=lra.IdRenjaAkhir AND lra.IdLokasi=l.IdLokasi 
AND ((l.IdKantor=2 AND l.IdInstansi=1 ) OR (l.IdKantor=3 AND l.IdInstansi IN (SELECT IdKelurahan FROM tbl_kelurahan_aktif WHERE Tahun=2019) 
AND l.IdInstansi IN (SELECT IdKelurahan FROM tbl_kelurahan WHERE IdKecamatan=1)))
AND prs.IdInstansi=skpd.IdSkpd AND skpd.IdSkpd IN (SELECT IdSkpd FROM tbl_skpd_aktif WHERE Tahun=2019)
GROUP BY lra.IdRenjaAkhir
ORDER BY skpd.KodeSkpd,pd.IdPrioritasDaerah,spd.IdSasaranPrioritasDaerah,bp.KodeProgram";
